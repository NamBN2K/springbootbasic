package com.example.springbootbasic.AutowiredPrimary;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
public class Girl {
    // khi thêm @Qualifier thì bỏ @Autoweied
    @Autowired
    Outfix outfix;

    // có thể @Autowired ở phương thức
//    public Girl(Outfix outfix) {
//        this.outfix = outfix;
//    }
    public Girl(@Qualifier("naked") Outfix outfix) {
        this.outfix = outfix;
    }
}
