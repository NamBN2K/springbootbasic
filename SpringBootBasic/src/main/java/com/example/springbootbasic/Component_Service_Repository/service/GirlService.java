package com.example.springbootbasic.Component_Service_Repository.service;

import com.example.springbootbasic.Component_Service_Repository.model.Girl;
import com.example.springbootbasic.Component_Service_Repository.repository.GirlRepository;
import org.apache.commons.lang.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Random;

/**
 * thực hiện nhiệm vụ giải quyết logic
 * giao tiếp với DB thông qua GirlRepository
 * **/
@Service
public class GirlService {
    @Autowired
    private GirlRepository girlRepository;

    public Girl getRandomGirl(){
        String name = randomGirlName(10);
        return girlRepository.getGrilByName(name);
    }

    public String randomGirlName(int length) {
        // Random một string có độ dài quy định
        // Sử dụng thư viện Apache Common Lang
        return RandomStringUtils.randomAlphanumeric(length).toLowerCase();
    }
}
