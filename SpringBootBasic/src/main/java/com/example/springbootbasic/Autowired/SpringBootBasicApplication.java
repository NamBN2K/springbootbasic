package com.example.springbootbasic.Autowired;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
public class SpringBootBasicApplication {

    public static void main(String[] args) {
        ApplicationContext context = SpringApplication.run(SpringBootBasicApplication.class, args);

        Outfix outfix = context.getBean(Outfix.class);
        System.out.println("Instance: " + outfix);
        outfix.wear();

        Girl girl = context.getBean(Girl.class);
        System.out.println("Girl instace: " + girl);
        System.out.println("Girl Outfix: " + girl.outfix);

        girl.outfix.wear();
    }
}
