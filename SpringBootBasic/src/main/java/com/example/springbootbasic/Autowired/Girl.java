package com.example.springbootbasic.Autowired;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class Girl {
    @Autowired
    Outfix outfix ;

    public Girl(Outfix outfix) {
        this.outfix = outfix;
    }
}