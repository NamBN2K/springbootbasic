package com.example.springbootbasic.ConfigurationAndBean;

public class MySqlConnector extends DataConnector{
    @Override
    public void connect() {
        System.out.println("Đã kết nối tới Mysql: " + getUrl());
    }
}
