package com.example.springbootbasic.ConfigurationAndBean;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
public class App {
    public static void main(String[] args) {
        ApplicationContext context = SpringApplication.run(App.class, args);
        DataConnector mysql = (DataConnector) context.getBean("mysqlConnector");
        mysql.connect();
        DataConnector postgresql = (DataConnector) context.getBean("postgreConnector");
        postgresql.connect();

    }
}
